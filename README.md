# N-Queens Problem


N-Queens problem is the problem of placing N chess queens on an NxN board so that no two queens attack each other.

My solution to the problem is using the Min-conflicts algorithm. 


## Running the program

When you run the program, you are asked to enter an integer N representing the number of queens and the width/height of the board. 

If you entered N > 4 && N < 100, the board will be drawn showing how the queens are positioned so as not to interact with each other. 
(it's a console application so you can't really see the board in the IDE when N is too big. :c)

If you entered N > 100, the board won't be drawn, but the time it took for the problem to be solved will be printed. 

My implementation, when compiled with g++ -O3 has the following results:
N = 10 000, time = 0.375sec
N = 15 000, time = 0.850sec
N = 25 000, time = 2.500sec

In the future I will see if it's possible to improve these times and aim for very few seconds when N = 100 000. 

