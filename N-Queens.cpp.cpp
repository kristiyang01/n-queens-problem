#include <iostream>
#include <cstdlib>
#include <vector>
#include <time.h>
using namespace std;

// the index in queen[] is the column, the value is the row of the queen.

// TO DO make so choosing queen column is random

void print(int *queen, const int &boardSize)
{
    for (int i = 0; i < boardSize; i++)
    {
        for (int j = 0; j < queen[i]; j++)
        {
            cout << "_ ";
        }
        cout << "* ";
        for (int j = queen[i] + 1; j < boardSize; j++)
        {
            cout << "_ ";
        }
        cout << endl;
    }
    return;
}

void initialize(int *queen, const int &boardSize, int *row_ElementCount, int *diag1_ElementCount, int *diag2_ElementCount)
{
    for (int i = 0; i < boardSize; i++)
    {
        queen[i] = rand() % boardSize;

        row_ElementCount[queen[i]]++;
        diag1_ElementCount[boardSize - 1 + i - queen[i]]++;
        diag2_ElementCount[i + queen[i]]++;
    }
}

void updateArrays(const int &boardSize, int col, int old_row, int new_row, int *row_ElementCount, int *diag1_ElementCount, int *diag2_ElementCount)
{
    row_ElementCount[old_row]--;
    diag1_ElementCount[boardSize - 1 + col - old_row]--;
    diag2_ElementCount[col + old_row]--;

    row_ElementCount[new_row]++;
    diag1_ElementCount[boardSize - 1 + col - new_row]++;
    diag2_ElementCount[col + new_row]++;
    return;
};

int QueenNumberOfConflicts(int *queen, const int &index, const int &boardSize, int *row_ElementCount, int *diag1_ElementCount, int *diag2_ElementCount)
{

    int result = 0;
    int row = queen[index];
    result = -3 + row_ElementCount[row] + diag1_ElementCount[boardSize - 1 + index - row] + diag2_ElementCount[index + row];
    return result;
}

// can be optimised
int getColOfQueenWithMaxConf(int *queen, const int &boardSize, int *row_ElementCount, int *diag1_ElementCount, int *diag2_ElementCount)
{
    int curMax = -1;
    int curMaxIndex;
    int curQueenNumConflicts;

    vector<int> maxConfIndexes;

    for (int i = 0; i < boardSize; i++)
    {
        curQueenNumConflicts = QueenNumberOfConflicts(queen, i, boardSize, row_ElementCount, diag1_ElementCount, diag2_ElementCount);

        if (curMax < curQueenNumConflicts)
        {
            curMax = curQueenNumConflicts;
            curMaxIndex = i;
            maxConfIndexes.clear();
            maxConfIndexes.push_back(i);
        }
        else
        {
            if (curMax == curQueenNumConflicts)
            {
                maxConfIndexes.push_back(i);
            }
        }
    }

    if (curMax == 0)
    {
        return -1;
    }

    if (maxConfIndexes.size() > 1)
    {
        curMaxIndex = maxConfIndexes[rand() % maxConfIndexes.size()];
    }

    return curMaxIndex;
}

int getRowWhereMinConf(int *queen, const int &queenIndex, const int &boardSize, int *row_ElementCount, int *diag1_ElementCount, int *diag2_ElementCount)
{
    int curQueenNumConflicts;
    int curMinConfRow;
    int curMinConf = boardSize + 1;
    vector<int> minConfIndexes;

    for (int i = 0; i < boardSize; i++)
    {
        updateArrays(boardSize, queenIndex, queen[queenIndex], i, row_ElementCount, diag1_ElementCount, diag2_ElementCount);
        queen[queenIndex] = i;
        curQueenNumConflicts = QueenNumberOfConflicts(queen, queenIndex, boardSize, row_ElementCount, diag1_ElementCount, diag2_ElementCount);

        if (curMinConf > curQueenNumConflicts)
        {
            curMinConf = curQueenNumConflicts;
            curMinConfRow = i;
            minConfIndexes.clear();
            minConfIndexes.push_back(i);
        }
        else
        {
            if (curMinConf == curQueenNumConflicts)
            {
                minConfIndexes.push_back(i);
            }
        }
    }
    if (minConfIndexes.size() > 1)
    {
        curMinConfRow = minConfIndexes[rand() % minConfIndexes.size()];
    }

    return curMinConfRow;
}

void printForTool(int *queen, const int &boardSize)
{
    cout << "[";
    for (int i = 0; i < boardSize - 1; i++)
    {
        cout << queen[i] << ", ";
    }
    cout << queen[boardSize - 1] << "]";

    return;
}

void solve(const int &boardSize)
{
    int *queen = new int[boardSize];

    int *row_ElementCount = new int[boardSize];
    int *diag1_ElementCount = new int[2 * boardSize - 1];
    int *diag2_ElementCount = new int[2 * boardSize - 1];

    for (int i = 0; i < boardSize; i++)
    {
        row_ElementCount[i] = 0;
    }

    for (int i = 0; i < 2 * boardSize - 1; i++)
    {
        diag1_ElementCount[i] = 0;
        diag2_ElementCount[i] = 0;
    }

    initialize(queen, boardSize, row_ElementCount, diag1_ElementCount, diag2_ElementCount);
    int col;
    int row;

    for (int i = 0; i <= 2 * boardSize; i++)
    {
        col = getColOfQueenWithMaxConf(queen, boardSize, row_ElementCount, diag1_ElementCount, diag2_ElementCount);

        if (col == -1)
        {
            // then there are no conflicts, so:
            break;
        }

        row = getRowWhereMinConf(queen, col, boardSize, row_ElementCount, diag1_ElementCount, diag2_ElementCount);

        updateArrays(boardSize, col, queen[col], row, row_ElementCount, diag1_ElementCount, diag2_ElementCount);
        queen[col] = row;
    }

    if (col != -1)
    { // if you didn't reach "break" because there are conflicts, then solve again.
        solve(boardSize);
    }
    else
    {
        if (boardSize <= 100)
        {
            print(queen, boardSize);
        }

        // if(boardSize<100) printForTool(queen, boardSize);
    }

    delete[] row_ElementCount;
    delete[] diag1_ElementCount;
    delete[] diag2_ElementCount;

    delete[] queen;
}

int main()
{
    srand(time(0));

    int queensCount;

    cout << "Enter queens count:" << endl;

    cin >> queensCount;

    if (queensCount < 4 && queensCount != 1)
    {
        cout << "-1" << endl;
        return 0;
    }

    clock_t begin = clock();

    solve(queensCount);

    clock_t end = clock();

    if (queensCount > 100)
    {
        cout << "It took " << (double)(end - begin) / 1000 << "seconds" << endl;
    }

    return 0;
}